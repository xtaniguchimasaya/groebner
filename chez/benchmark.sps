#!/usr/bin/env chez --script

(import (chezscheme)
	(parser)
	(printer)
	(groebner)
	(ordering)
	(variables))

(define (benchmark)
  (define f (parse "u^2+u^2-1"))
  (define g (parse "1/9*u^4-2/9*u^3*x+5/18*u^2*v*y+1/9*u^2*x^2-5/18*u*v*x*y+1/9*v^2*y^2+1/4*u^2*v^2+1/4*u^2*y^2-1/2*u*v^2*x+1/4*v^2*x^2-1"))
  (define h (parse "-5/9*v^2*y*u-5/18*v*y^2*u+5/18*v^2*y*x+4/9*ux-u*u*v+2/9*ux-u*u*y-2/9*ux-u*v*x+1/2*u^3*v+1/2*u^3*y-u^2*v*x-1/2*u^2*x*y-1/2*u*v^3+1/2*u*v*x^2+1/2*v^3*x"))
  (display (map print (groebner+opt f g h))))

(set-ordering! grevlex)
(set-variables! "xyz")
(time (benchmark))

