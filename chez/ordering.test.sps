#!/usr/bin/env chez --script

(import (ordering)
	(variables)
	(parser))

(set-variables! "xyz")

(define p parse)
(define m '#(2 1 1))
(define n '#(3 0 0))
(define o '#(1 3 0))

(assert (lex n m))
(assert (lex m o))

(assert (grlex m o))
(assert (grlex o n))

(assert (grevlex o m))
(assert (grevlex m n))

(set-ordering! lex)
(assert (equal? (lt (p "x*y+1")) '(#(1 1 0) . 1)))
(assert (equal? (lt (p "y+1")) '(#(0 1 0) . 1)))
(assert (equal? (lm (p "x*y+1")) '#(1 1 0)))
(assert (equal? (lc (p "x*y+1")) 1))
