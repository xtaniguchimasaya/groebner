#!r6rs
(library (ordering)
  (export lex revlex grlex grevlex lt lc lm ordering set-ordering!)
  (import (rnrs))
  
  (define (lex a b)
    (let loop ([i 0])
      (if (= (vector-ref a i) (vector-ref b i))
	  (and (< (+ i 1) (vector-length a)) (loop (+ i 1)))
	  (> (vector-ref a i) (vector-ref b i)))))
  
  (define (revlex a b)
    (let loop ([i (- (vector-length a) 1)])
      (if (= (vector-ref a i) (vector-ref b i))
	  (and (> i 0) (loop (- i 1)))
	  (< (vector-ref a i) (vector-ref b i)))))
  
  (define (grlex a b)
    (let ([c (apply + (vector->list a))]
	  [d (apply + (vector->list b))])
      (if (= c d) (lex a b) (> c d))))
  
  (define (grevlex a b)
    (let ([c (apply + (vector->list a))]
	  [d (apply + (vector->list b))])
      (if (= c d) (revlex a b) (> c d))))

  (define ord lex)
  
  (define (ordering) ord)
  
  (define (set-ordering! ordering)
    (set! ord ordering))
  
  (define (gt a b)
    (if ((ordering) (car a) (car b)) a b))
  
  (define (lt p)
    (if (>= 1 (length p))
	(car p)
	(fold-left gt (car p) (cdr p))))
  
  (define (lm p)
    (car (lt p)))
  
  (define (lc p)
    (cdr (lt p)))
  
  )
