#!/usr/bin/env chez --script

(import (division)
	(parser)
	(printer)
	(polynomial))

(define p parse)

(assert (p= (remainder (p "x+y") (p "x")) (p "y")))
(assert (p= (quotients (p "x+y") (p "x")) (list (p "1"))))
(display (print (remainder (p "x*z+y*z+z") (p "z") (p "x+y"))))
(assert (p= (remainder (p "x*z+y*z+z") (p "z") (p "x+y")) '()))
