(in-package :cl)
(defpackage :groebner.term.group
  (:use :cl :groebner.monomial.group)
  (:export t* t/))
(in-package :groebner.term.group)

(defun t* (m n)
  (cons
   (* (first m) (first n))
   (m* (rest m) (rest n))))

(defun t/ (m n)
  (cons
   (/ (first m) (first n))
   (m/ (rest m) (rest n))))
