(in-package :cl)
(defpackage :groebner.polynomial.utility
  (:use :cl
        :groebner.polynomial.ring
        :groebner.monomial.group)
  (:export lt))
(in-package :groebner.polynomial.utility)

