(in-package :cl)
(defpackage :groebner.asd
  (:use :cl :asdf))
(in-package :groebner.asd)

(defsystem groebner
  :serial t
  :depends-on (:esrap :cl-ppcre)
  :components 
  ((:module monomial
    :components ((:file "group")))
   (:module term
    :components ((:file "group")))
   (:module polynomial
    :components ((:file "ring")))
   (:file "parser")
   (:file "printer")
   (:file "package")))
