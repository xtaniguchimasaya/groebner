(in-package :cl)
(defpackage :earlgray.parser
  (:use :cl :earlgray.polynomial :earlgray.monomial)
  (:export parse))
(in-package :earlgray.parser)

(defun rough-split-string (delimiters string)
  (loop
     :with test := (lambda (c) (member c delimiters))
     :for start := 0 :then end
     :for end := (position-if test string :start (1+ start))
     :when (and (zerop start) (not end)) :return (list string)
     :unless end :return (append chunks (list (subseq string start end)))
     :unless (= start end) :collecting (subseq string start end) :into chunks))

(defun split-string (delimiters string)
  (loop
     :with test := (lambda (c) (member c delimiters))
     :for start := 0 :then (1+ end)
     :for end := (position-if test string :start start)
     :when (and (zerop start) (not end)) :return (list string)
     :unless end :return (append chunks (list (subseq string start end)))
     :unless (= start end) :collecting (subseq string start end) :into chunks))

(defun parse-monomial (string)
  (let ((exponents (make-array (length +variables+) :initial-element 0)))
    (dolist (variable (split-string '(#\*) string))
      (if (find #\^ variable)
	  (incf (svref exponents (position (char variable 0) +variables+)) (parse-integer (subseq variable 2)))
	  (incf (svref exponents (position (char variable 0) +variables+)) 1)))
    (make-monomial :exponents exponents)))

(defun parse-term (string)
  (when (char= #\+ (char string 0))
    (setf string (subseq string 1)))
  (if (char= #\- (char string 0))
      (if (digit-char-p (char string 1))
	  (if (position #\* string)
	      (let ((c (subseq string 0 (position #\* string)))
		    (m (subseq string (position #\* string))))
		(cons (read-from-string c) (parse-monomial m)))
	      (cons (read-from-string string) (make-monomial)))
	  (let ((m (subseq string 1)))
	    (cons -1 (parse-monomial m))))
      (if (digit-char-p (char string 0))
	  (if (position #\* string)
	      (let ((c (subseq string 0 (position #\* string)))
		    (m (subseq string (position #\* string))))
		(cons (read-from-string c) (parse-monomial m)))
	      (cons (read-from-string string) (make-monomial)))
	  (let ((m (subseq string 0)))
	    (cons 1 (parse-monomial m))))))

(defun parse-polynomial (string)
  (make-polynomial :terms (mapcar #'parse-term (rough-split-string '(#\+ #\-) string))))

(defmethod parse ((string string))
  (parse-polynomial string))

(defmethod parse ((number number))
  (parse-polynomial (princ-to-string number)))

;; (set-macro-character #\$ (lambda (s c) (list 'parse (read s t nil t))))
(set-macro-character #\$
   (lambda (stream char)
     (loop
	:for c := (read-char stream) :until (char= #\$ c)
	:collecting c :into s
	:finally (return (list 'parse (coerce s 'string))))))
