(in-package :cl)
(defpackage :earlgray.monomial
  (:use :cl :earlgray.operator)
  (:export make-monomial
	   monomial-exponents
	   lexicographical-order
	   degree
	   +variables+))
(in-package :earlgray.monomial)

(defvar +variables+ "abcdefghijklmnopqrstuvwxyz")

(defstruct (monomial (:print-object print-monomial))
  (exponents (make-array (length +variables+) :initial-element 0 :element-type 'fixnum)))

(defun print-monomial (monomial stream)
  (loop :with k := (position-if-not #'zerop (monomial-exponents monomial))
        :for exponent :across (monomial-exponents monomial)
        :for i :from 0 :unless (zerop exponent) :do
        (format stream "~:[*~a~;~a~]~:[^~a~;~*~]"
		(= i k) (char +variables+ i)
		(= 1 exponent) exponent)))

(defmethod lexicographical-order ((a monomial) (b monomial))
  (loop :for ai :across (monomial-exponents a)
        :for bi :across (monomial-exponents b)
        :when (< ai bi) :return t
        :when (> ai bi) :return nil))

(defmethod degree ((a monomial))
  (reduce #'+ (monomial-exponents a)))

(defmethod power ((a monomial) (b number))
  (make-monomial :exponents (map 'vector (lambda (c) (* c b)) (monomial-exponents a))))

(defmethod multiply ((a monomial) (b monomial))
  (make-monomial :exponents (map 'vector #'+ (monomial-exponents a) (monomial-exponents b))))

(defmethod divide ((a monomial) (b monomial))
  (make-monomial :exponents (map 'vector #'- (monomial-exponents a) (monomial-exponents b))))
