(in-package :cl)
(defpackage :earlgray.prime
  (:use :cl)
  (:export primes
	   most-positive-prime
	   primep
	   factorize
	   factorial
	   combination))
(in-package :earlgray.prime)

(defun bsearch (ary key imin imax)
  (declare (fixnum key imin imax) (simple-vector ary))
  (if (< imax imin)
      nil
      (let ((imid (+ imin (floor (- imax imin) 2))))
	(cond ((> (svref ary imid) key)
	       (bsearch ary key imin (1- imid)))
	      ((< (svref ary imid) key)
	       (bsearch ary key (1+ imid) imax))
	      (t imid)))))

(defmacro generate-primes ()
  (loop
     :for n :from 3 :to 10000000 :by 2
     :for m := (isqrt n)
     :when (loop :for d :in primes :while (<= d m) :never (zerop (mod n d)))
     :collecting n :into primes
     :finally (return `(coerce '(2 ,@primes) 'vector))))

(defvar primes (generate-primes))

(defvar most-positive-prime (svref primes (1- (length primes))))

(defun primep (n)
  (if (<= n most-positive-prime)
      (bsearch primes n 0 (1- (length primes)))
      (and 
       (loop
	  :with m := (1+ (isqrt n))
	  :for p :across primes :while (<= p m)
	  :never (zerop (mod n p)))
       (loop
	  :with m := (1+ (isqrt n))
	  :for d :from most-positive-prime :by 2 :while (<= d m)
	  :never (zerop (mod n d))))))

(defun factorize (n)
  (let (factors (m n))
    (loop
       :for p :across primes
       :when (> p (1+ (isqrt m))) :do
       (push m factors)
       (return-from factorize factors)
       :when (zerop (mod m p)) :do
       (loop
	  :while (zerop (mod m p)) :do
	  (push p factors)
	  (setf m (/ m p))))
    (loop
       :for d :from most-positive-prime :by 2
       :until (= m 1)
       :when (> d (1+ (isqrt m))) :do
       (push m factors)
       (return-from factorize factors)
       :when (zerop (mod m d)) :do
       (loop
	  :while (zerop (mod m d)) :do
	  (push d factors)
	  (setf m (/ m d))))
    factors))

(defun factorial (n &optional &key (start 1))
  (do* ((i start (1+ i)) (j i (* i j))) ((>= i n) j)))

(defun combination (n k)
  (let ((m (min k (- n k))))
    (/ (factorial n :start (- n m -1)) (factorial m))))

