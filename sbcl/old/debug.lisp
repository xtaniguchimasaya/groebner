(in-package :cl)
(defpackage :earlgray.debug (:use :cl))
(in-package :earlgray.debug)

(set-dispatch-macro-character
 #\# #\!
 (lambda (stream c1 c2)
   (list (read stream) (read stream t nil t))))
